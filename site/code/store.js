const store = JSON.parse(localStorage.store);
console.log(store);

if (!Array.isArray(store)) {
  throw Error("Sosmething is wrong...");
}

const storeEl = store.map(
  ({
    productImage,
    productName,
    productDescription,
    keywords,
    porductPrice,
    id,
  }) => {
    return `
    <div class="store">
  <h3 class="store-name">${productName}</h3>
  <img src="${productImage}" alt="${productName}" id='${id}'>
  <p class="store-description">${productDescription}</p>
  <div>
    <span class="badge bg-secondary">${keywords}</span>
  </div>
  <p class="store-price">${porductPrice}</p>
</div>
  `;
  }
);

document
  .querySelector(".store-box")
  .insertAdjacentHTML("beforeend", storeEl.join(""));
